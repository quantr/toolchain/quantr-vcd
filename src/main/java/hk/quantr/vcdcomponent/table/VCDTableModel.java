package hk.quantr.vcdcomponent.table;

import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.assembler.exception.NoOfByteException;
import hk.quantr.assembler.exception.WrongInstructionException;
import hk.quantr.assembler.riscv.il.Line;
import hk.quantr.javalib.CommonLib;
import hk.quantr.vcd.datastructure.VCD;
import hk.quantr.vcd.datastructure.Wire;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VCDTableModel extends AbstractTableModel {

	public VCD vcd;
	public ArrayList<Wire> wires = new ArrayList<>();
	public ArrayList<Wire> filterdWires = new ArrayList<>();
	public boolean showRiseClockOnly;
	int clkCol;
	HashMap<String, String> cache = new HashMap<>();
	public int noOfRow;
	public boolean signed;

	RISCVDisassembler disassembler = new RISCVDisassembler();
	public HashMap<Pair<Integer, Integer>, ArrayList<Pair<Integer, Integer>>> checkpoints = new HashMap<>();
	public String type = "hex";

	public void init(VCD vcd) {
		this.vcd = vcd;
		this.fireTableStructureChanged();
	}

	public void clearCache() {
		cache.clear();
	}

	@Override
	public int getRowCount() {
		if (filterdWires.isEmpty()) {
			return 0;
		} else {
			if (noOfRow == 0) {
				return filterdWires.get(0).values.size();
			}
			return Math.min(noOfRow, filterdWires.get(0).values.size());
		}
	}

	@Override
	public int getColumnCount() {
		return wires.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		String key = columnIndex + "_" + rowIndex;
		if (cache.containsKey(key)) {
			return cache.get(key);
		} else {
			BigInteger val = filterdWires.get(columnIndex).values.get(rowIndex);
			if (getColumnName(columnIndex).equals("inst")) {
				String str = "";
				try {
					disassembler.disasm(new ByteArrayInputStream(CommonLib.getByteArray(val.intValue())));
					for (Line line : disassembler.disasmStructure.lines) {
						str += line.code;
						if (line.code.equals("unimp")) {
							break;
						}
					}
				} catch (NoOfByteException | WrongInstructionException | IOException ex) {
					Logger.getLogger(VCDTableModel.class.getName()).log(Level.SEVERE, null, ex);
				}
				String r = "<html><body>" + val.toString(16) + " <span style=\"font-weight: bold\">" + str + "</span></body></html>";
//				String r = val.toString(16);
				cache.put(key, r);
				return r;
			} else {
				if (signed) {
					if (type.equals("dec")) {
						cache.put(key, BigInteger.valueOf(val.longValue()).toString());
						return BigInteger.valueOf(val.longValue()).toString();
					} else {
						cache.put(key, BigInteger.valueOf(val.longValue()).toString(16));
						return BigInteger.valueOf(val.longValue()).toString(16);
					}
				} else {
					if (type.equals("dec")) {
						cache.put(key, val.toString());
						return val.toString();
					} else {
						cache.put(key, val.toString(16));
						return val.toString(16);
					}
				}
			}
		}
	}

	public BigInteger geBigIntegerAt(int rowIndex, int columnIndex) {
		BigInteger val = filterdWires.get(columnIndex).values.get(rowIndex);
		return val;
	}

	@Override
	public String getColumnName(int column) {
		return wires.get(column).name.replace("(", "[").replace(")", "]");
	}

	public Wire getColumnWire(int column) {
		return wires.get(column);
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return Long.class;
	}

	private ArrayList<Integer> getClkRiseRowNo() {
		ArrayList<Integer> temp = new ArrayList<>();
		int x = 0;
		clkCol = -1;
		for (Wire wire : wires) {
			if (wire.name.equals("clk")) {
				clkCol = x;
				for (int z = 0; z < wire.values.size(); z++) {
					if (wire.values.get(z).longValue() == 1) {
						temp.add(z);
					}
				}
				break;
			}
			x++;
		}
		return temp;
	}

//	private ArrayList<Integer> getClkRow() {
//		ArrayList<Integer> temp = new ArrayList<>();
//		for (int x = 0; x < getRowCount(); x++) {
//			if ((Long) getValueAt(x, clkCol) == 1) {
//				temp.add(x);
//			}
//		}
//		return temp;
//	}
	public void filter() {
		filterdWires.clear();
		ArrayList<Integer> temp = getClkRiseRowNo();
		for (Wire wire : wires) {
			try {
				Wire newWire = (Wire) wire.clone();
				if (showRiseClockOnly) {
					newWire.values.clear();
					for (int x : temp) {
						newWire.values.add(wire.values.get(x));
					}
				}
				filterdWires.add(newWire);
			} catch (CloneNotSupportedException ex) {
				Logger.getLogger(VCDTableModel.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		cache.clear();
	}

	public Wire getWire(String scopeName, String wireName) {
		for (int x = 0; x < getColumnCount(); x++) {
			Wire wire = wires.get(x);
			if (wire.scope.name.equals(scopeName) && wire.name.equals(wireName)) {
				return wire;
			}
		}
		return null;
	}

	public int getColNo(String scopeName, String wireName) {
		for (int x = 0; x < getColumnCount(); x++) {
			Wire wire = wires.get(x);
			if (wire.scope.name.equals(scopeName) && getColumnName(x).equals(wireName)) {
				return x;
			}
		}
		return -1;
	}

}
