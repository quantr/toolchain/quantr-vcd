package hk.quantr.vcdcomponent.tree;

import hk.quantr.vcdcomponent.QuantrVCDComponent;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.JTree;
import javax.swing.event.ChangeEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreePath;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CheckBoxNodeEditor extends AbstractCellEditor implements TreeCellEditor {

	VCDTreeCellRenderer renderer = new VCDTreeCellRenderer();
	ChangeEvent changeEvent = null;
	JTree tree;
	MyPanel panel;
	QuantrVCDComponent quantrVCDComponent;

	public CheckBoxNodeEditor(JTree tree, QuantrVCDComponent quantrVCDComponent) {
		this.tree = tree;
		this.quantrVCDComponent = quantrVCDComponent;
	}

	@Override
	public Object getCellEditorValue() {
		TreePath path = tree.getSelectionPath();
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
		CheckBoxNode c = (CheckBoxNode) node.getUserObject();
		c.selected = panel.checkbox.isSelected();
		return c;
	}

	@Override
	public boolean isCellEditable(EventObject event) {
		return true;
	}

	@Override
	public Component getTreeCellEditorComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row) {
		panel = (MyPanel) renderer.getTreeCellRendererComponent(tree, value, true, expanded, leaf, row, true);
		panel.setOpaque(true);
		ItemListener itemListener = new ItemListener() {
			public void itemStateChanged(ItemEvent itemEvent) {
//				if (stopCellEditing()) {
				fireEditingStopped();
				quantrVCDComponent.updateJTreeAfterSelect(null);
//				}
			}
		};

		if (panel.checkbox.getItemListeners().length == 0) {
			panel.checkbox.addItemListener(itemListener);
		}
		if (panel.label.getMouseListeners().length == 0) {
			panel.label.addMouseListener(new MouseListener() {
				@Override
				public void mouseClicked(MouseEvent me) {
					panel.checkbox.setSelected(!panel.checkbox.isSelected());
				}

				@Override
				public void mousePressed(MouseEvent me) {
				}

				@Override
				public void mouseReleased(MouseEvent me) {
				}

				@Override
				public void mouseEntered(MouseEvent me) {
				}

				@Override
				public void mouseExited(MouseEvent me) {
				}
			});
		}
		return panel;
	}

}
