package hk.quantr.vcdcomponent;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class JComboBoxFontRenderer extends BasicComboBoxRenderer {

	protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
	JLabel label = new JLabel();

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
//		JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		Font font = (Font) value;
		if (font==null){
			return label;
		}
		label.setText(font.getFontName());
		if (isSelected) {
			label.setBackground((Color) UIManager.get("ComboBox.selectionBackground"));
		} else {
			label.setBackground((Color) UIManager.get("ComboBox.background"));
		}
		label.setFont(font.deriveFont(Font.PLAIN, 16));
		return label;
	}
}
