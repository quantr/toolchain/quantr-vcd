package hk.quantr.vcdcomponent;

import hk.quantr.vcdcheck.MyVCDCheckListener;
import hk.quantr.vcdcheck.VCDCheck;
import hk.quantr.vcdcheck.structure.Statement;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Test1 {

	@Test
	public void test() throws FileNotFoundException, IOException {
		MyVCDCheckListener listener = VCDCheck.check(IOUtils.toString(new FileReader("c:\\workspace\\quantr-i\\output.vc")));
		ArrayList<Statement> statements = listener.statements;
		System.out.println("statements.size()=" + statements.size());
	}
}
